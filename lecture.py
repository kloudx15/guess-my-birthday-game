# Get the game started: print out the game introduction
input("choose a number between 1 and 50 (press enter when ready)\n")

# variables for game state
low = 1  # low limit for possible guesses
high = 50  # high limit for possible guesses

# calculate the next guess
guess = int((low + high) / 2)

# print out the guess and get player's response
print("\nIs", guess, "your number? (correct, higher, lower)")
answer = input("Answer: ")

if answer == "correct":
    print("\nThe computer guessed your number: ", guess)
elif answer == "lower":
    # if the answer is too high
    high = guess - 1
elif answer == "higher":
    # if the answer is too low
    low = guess + 1
else:
    # unknown input
    print("\nbad input try again\n")

print("\nThe computer didn't guess your number, you win!")